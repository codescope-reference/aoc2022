use std::collections::HashMap;

#[derive(Debug)]
enum Element {
    Dir(String),
    File(String, usize),
}
use Element::*;

pub fn solution() {
    let input = std::fs::read_to_string("../data/day07/input.txt").unwrap();

    let lines = input.split("\n").filter(|l| l.len() > 0);

    let mut dir_stack = vec![];
    let mut filesystem = HashMap::<String, Vec<Element>>::new();

    for line in lines {
        match line.split(" ").collect::<Vec<&str>>().as_slice() {
            ["$", "cd", ".."] => {
                dir_stack.pop();
            }
            ["$", "cd", folder] => {
                dir_stack.push(folder.to_string());
            }
            ["$", "ls"] => (),
            ["dir", name] => {
                let current_dir = dir_stack.join("-");
                let full_name = format!("{current_dir}-{name}");
                let current_contents = filesystem.get_mut(&current_dir);

                match current_contents {
                    None => {
                        filesystem.insert(current_dir, vec![Dir(full_name)]);
                    }
                    Some(contents) => {
                        contents.push(Dir(full_name));
                    }
                }
            }
            [size, filename] => {
                let current_dir = dir_stack.join("-");
                let current_contents = filesystem.get_mut(&current_dir);

                let size_int = size.parse::<usize>().expect("Could not parse filesize");
                let file = File(filename.to_string(), size_int);
                match current_contents {
                    None => {
                        filesystem.insert(current_dir, vec![file]);
                    }
                    Some(contents) => {
                        contents.push(file);
                    }
                }
            }
            _other => (),
        }
    }

    let mut total_size = 0;
    for key in filesystem.keys() {
        let folder_size = calculate_folder_size(key, &filesystem);
        if folder_size <= 100000 {
            total_size += folder_size;
        }
    }
    println!("Total size of folders less than 100k in size: {total_size}");

    let used_size = calculate_folder_size("/", &filesystem);
    let free_space = 70000000 - used_size;
    let need_free = 30000000 - free_space;

    let mut smallest = None;
    let mut smallest_size = 0;

    for key in filesystem.keys() {
        let folder_size = calculate_folder_size(key, &filesystem);
        match smallest {
            None => {
                if folder_size >= need_free {
                    smallest = Some(key);
                    smallest_size = folder_size;
                }
            }
            Some(_) => {
                if folder_size <= smallest_size && folder_size >= need_free {
                    smallest = Some(key);
                    smallest_size = folder_size;
                }
            }
        }
    }

    println!(
        "Should delete directory {:?} with size {}",
        smallest.unwrap(),
        smallest_size
    );
}

fn calculate_folder_size(folder: &str, filesystem: &HashMap<String, Vec<Element>>) -> usize {
    if let Some(contents) = filesystem.get(folder) {
        let mut total_size = 0;
        for element in contents {
            match element {
                File(_, size) => total_size += size,
                Dir(full_name) => total_size += calculate_folder_size(full_name, filesystem),
            }
        }

        total_size
    } else {
        0
    }
}
